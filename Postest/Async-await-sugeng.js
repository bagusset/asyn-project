const express = require("express");
const bcrypt = require("bcrypt");

const app = express();
const port = process.env.PORT || 3000;

const { Book, User, Todo } = require("./models");

app.use(express.json());

app.get("/", (req, res) => {
  res.status(200).json({
    status: true,
    message: "Tugas TED Express Sequelize using Async Await",
  });
});

app.get("/books", async (req, res) => {
  try {
    const books = await Book.findAll();
    res.status(200).json({
      status: true,
      message: "All Books retrieved",
      data: books,
    });
  } catch (error) {
    res.send(error);
  }
});

app.get("/books/:id", async (req, res) => {
  try {
    const book = await Book.findByPk(req.params.id);
    res.status(200).json({
      status: true,
      message: `Book with id ${req.params.id} retrieved`,
      data: book,
    });
  } catch (error) {
    res.send(error);
  }
});

app.post("/books", async (req, res) => {
  try {
    const book = await Book.create({
      title: req.body.title,
      author: req.body.author,
    });
    res.status(201).json({
      status: true,
      message: "Books created!",
      data: book,
    });
  } catch (error) {
    res.send(error);
  }
});

app.put("/books/:id", async (req, res) => {
  try {
    const book = await Book.findByPk(req.params.id);
    const updatedBook = await book.update(
      {
        title: req.body.title || book.title,
        author: req.body.author || book.author,
      },
      {
        where: {
          id: req.params.id,
        },
      }
    );
    res.status(201).json({
      status: true,
      message: `Books with id ${req.params.id} updated!`,
      data: updatedBook,
    });
  } catch (error) {
    res.send(error);
  }
});

app.delete("/books/:id", async (req, res) => {
  try {
    await Book.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(201).json({
      status: true,
      message: `Books with id ${req.params.id} deleted!`,
    });
  } catch (error) {
    res.send(error);
  }
});

app.get("/users", async (req, res) => {
  try {
    const users = await User.findAll({
      attributes: {
        exclude: ["password"],
      },
    });
    if (users.length !== 0) {
      res.status(230).json({
        status: true,
        message: "All Users retrieved",
        data: users,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Users is empty`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.get("/users/:id", async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id, {
      include: [
        {
          model: Todo,
          as: "todos",
        },
      ],
      attributes: {
        exclude: ["password"],
      },
    });

    if (user && user.id) {
      res.status(230).json({
        status: true,
        message: `User with id ${req.params.id} retrieved`,
        data: user,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `User with id ${req.params.id} not found`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.post("/users", async (req, res) => {
  try {
    const hash = await bcrypt.hash(req.body.password, 10);
    const user = await User.create({
      name: req.body.name,
      email: req.body.email,
      password: hash,
    });
    res.status(231).json({
      status: true,
      message: "User has been created!",
      data: user,
    });
  } catch (error) {
    res.send(error);
  }
});

app.put("/users/:id", async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id);
    if (user && user.id) {
      const hash = req.body.password
        ? await bcrypt.hash(req.body.password, 10)
        : user.password;

      const updatedUser = await user.update(
        {
          name: req.body.name || user.name,
          email: req.body.email || user.email,
          password: hash,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );

      res.status(232).json({
        status: true,
        message: `User with id ${req.params.id} updated!`,
        data: updatedUser,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `User with id ${req.params.id} not found!`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.delete("/users/:id", async (req, res) => {
  try {
    const user = await User.destroy({
      where: {
        id: req.params.id,
      },
    });

    if (user) {
      res.status(233).json({
        status: true,
        message: `User with id ${req.params.id} deleted!`,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `User with id ${req.params.id} not found!`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.get("/todos", async (req, res) => {
  try {
    const todos = await Todo.findAll();
    if (todos.length !== 0) {
      res.status(230).json({
        status: true,
        message: "All Todos retrieved",
        data: todos,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todos is empty`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.get("/todos/:id", async (req, res) => {
  try {
    const todo = await Todo.findByPk(req.params.id, {
      include: [
        {
          model: User,
          as: "user",
        },
      ],
    });

    if (todo && todo.id) {
      res.status(230).json({
        status: true,
        message: `Todo with id ${req.params.id} retrieved`,
        data: todo,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todo with id ${req.params.id} not found`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.post("/todos", async (req, res) => {
  try {
    const user = await User.findByPk(req.body.user_id);
    if (user && user.id) {
      const todo = await Todo.create({
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id: user.id,
      });
      res.status(231).json({
        status: true,
        message: `Todo has been added for User ${user.name}`,
        data: todo,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Add todo failed due to User with id ${req.body.user_id} not found!`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.put("/todos/:id", async (req, res) => {
  try {
    if (req.body.user_id) {
      const user = await User.findByPk(req.body.user_id);
      if (user && user.id) {
        const todo = await Todo.findByPk(req.params.id);
        if (todo && todo.id) {
          const updatedTodo = await todo.update(
            {
              name: req.body.name || todo.name,
              description: req.body.description || todo.description,
              due_at: req.body.due_at || todo.due_at,
              user_id: req.body.user_id || todo.user_id,
            },
            {
              where: {
                id: req.params.id,
              },
            }
          );
          res.status(232).json({
            status: true,
            message: `Todo with id ${req.params.id} updated and have new assigned to User ${user.name}`,
            data: updatedTodo,
          });
        } else {
          res.status(244).json({
            status: false,
            message: `Todo with id ${req.params.id} not found!`,
          });
        }
      } else {
        res.status(244).json({
          status: false,
          message: `Update todo failed due to User with id ${req.body.user_id} not found!`,
        });
      }
    } else {
      const todo = await Todo.findByPk(req.params.id);
      if (todo && todo.id) {
        await todo.update(
          {
            name: req.body.name || todo.name,
            description: req.body.description || todo.description,
            due_at: req.body.due_at || todo.due_at,
          },
          {
            where: {
              id: req.params.id,
            },
          }
        );
        res.status(232).json({
          status: true,
          message: `Todo with id ${req.params.id} updated!`,
          data: todo,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Todo with id ${req.params.id} not found!`,
        });
      }
    }
  } catch (error) {
    res.send(error);
  }
});

app.delete("/todos/:id", async (req, res) => {
  try {
    const todo = await Todo.destroy({
      where: {
        id: req.params.id,
      },
    });
    if (todo) {
      res.status(233).json({
        status: true,
        message: `Todo with id ${req.params.id} deleted!`,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Todo with id ${req.params.id} not found!`,
      });
    }
  } catch (error) {
    res.send(error);
  }
});

app.listen(port, () => console.log(`Listen on port ${port} using async await`));
